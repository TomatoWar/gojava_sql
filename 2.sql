-- searching for the most expensive project (based on developers salary)

CREATE TABLE projects_costs (
	salary_sum MEDIUMINT,
	project_name VARCHAR(20)
);

INSERT INTO projects_costs (salary_sum, project_name)
SELECT SUM(developers.salary) AS sum_salary, projects.name AS project_name
		FROM developers
		INNER JOIN projects ON developers.project_id=projects.id
		GROUP BY project_id;

SELECT salary_sum, project_name
FROM projects_costs
WHERE salary_sum = (select MAX(salary_sum) from projects_costs);

-- getting rid of the temporary table

DROP TABLE projects_costs;