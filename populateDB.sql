INSERT INTO companies (name)
VALUES ("google"),
	("facebook"),
	("oracle"),
	("yandex");

INSERT INTO customers (name)
VALUES ("WorldViz"),
	("Tion"),
	("SpaceX"),
	("Sberbank"),
	("Mfc-capital");

INSERT INTO projects (name, company_id, customer_id)
VALUES ("vr_glasses", 1, 1),
	("breezer 3s", 4, 2),
	("clever MAC", 2, 2),
	("mars_land", 3, 3),
	("super_db", 3, 4),
	("robot-broker", 1, 5);

INSERT INTO developers (name, project_id, company_id)
VALUES ("Denis Ivashkov", 1, 1),
	("Linus Torwalds", 3, 2),
	("Mark Zuckerberg", 2, 4),
	("Ilon Mask", 3, 2),
	("Geib Newell", 4, 3),
	("Axel Rose", 5, 3),	
	("Van Hallen", 6, 1);

INSERT INTO skills (name)
VALUES ("Java SE"),
	("Java EE"),
	("JavaScript"),
	("C++"),
	("SQL"),
	("MySQL"),
	("Git"),
	("HTML"),
	("Spring");

INSERT INTO developers_skills
VALUES (1, 1),
	(1, 2),
	(1, 5),
	(1, 6),
	(2, 1),
	(3, 1),
	(3, 2),
	(4, 4),
	(5, 2),
	(6, 1),
	(6, 3),
	(7, 1);