-- calculating the sum of all java-developers salaries

SELECT SUM(dev_salary) AS sum_of_java_devs_salaries 
FROM 
(
	SELECT developers.id AS dev_id, skills.name AS skill_name, developers.salary AS dev_salary
	FROM 
	((developers LEFT JOIN developers_skills ON developers.id=developers_skills.dev_id)
	INNER JOIN skills ON developers_skills.skill_id=skills.id)
WHERE skills.name = 'Java SE' OR skills.name = 'Java EE'
GROUP BY developers.id
) AS salaries_of_java_devs;

