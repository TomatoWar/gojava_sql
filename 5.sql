-- adding a few rows into the projects table to have more representative statistics

INSERT INTO projects (name, company_id, customer_id, cost)
VALUES ("next-gen_drones", 2, 1, 10000000),
	("5d-ct", 2, 2, 15000000),
	("360-degrees-camera", 3, 3, 6000000),
	("new-basic", 3, 4, 1500000),
	("ik-module", 1, 3, 4000000),
	("magic_air", 4, 5, 2500000),
	("boneless_pizza", 3, 1, 2000000),
	("rave_pary", 4, 4, 35000000);

-- searching for the least profitable customer for each company

-- creating table with all projects in a way: company-cutomer-project

CREATE VIEW cash_flow AS
SELECT companies.name AS company, customers.name AS customer, projects.cost
FROM ((projects
LEFT JOIN companies ON projects.company_id = companies.id)
LEFT JOIN customers ON projects.customer_id = customers.id);

-- calculating the overall cost of every customer of each company

CREATE VIEW cash_flow_grouped AS
SELECT companies.name AS company, customers.name AS customer, SUM(projects.cost) AS cost
FROM ((projects
LEFT JOIN companies ON projects.company_id = companies.id)
LEFT JOIN customers ON projects.customer_id = customers.id)
GROUP BY company, customer
ORDER BY company;

-- calculating the least profitable customer for each company

SELECT A.company AS company, A.customer AS customer, A.cost as cost
FROM  cash_flow_grouped A
INNER JOIN
(
	SELECT MIN(cost) AS min_cost, company
	FROM cash_flow_grouped
	GROUP BY company
) B ON A.company = B.company AND A.cost = B.min_cost
ORDER BY A.company;

-- droping temporary views

DROP VIEW cash_flow;
DROP VIEW cash_flow_grouped;