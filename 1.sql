-- adding "salary" field to developers table

ALTER TABLE developers
ADD salary MEDIUMINT NOT NULL;

UPDATE developers
SET salary = 600;

UPDATE developers
SET salary = 1200
WHERE id = 1;

UPDATE developers
SET salary = 4000
WHERE name = "Geib Newell";

UPDATE developers
SET salary = 2200
WHERE name = "Axel Rose";

-- tracking the result
SELECT id, name, salary FROM developers;