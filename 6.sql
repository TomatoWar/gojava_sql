-- providing each project with extra developers so we could calculate average salary

INSERT INTO developers (name, project_id, company_id, salary)
VALUES ("SideShow Bob", 2, 4, 600), ("Marren Air", 7, 2, 700), ("Jack Sparrow", 8, 2, 800),
("Lady GaGa", 9, 3, 900), ("Denis Korpusov", 10, 3, 1000), ("Ivan Chervoniy", 11, 1, 1200),
("Gryazniy Vonniy", 12, 4, 1400), ("Sara Kerrigan", 13, 3, 1500), ("Jim Raynor", 14, 4, 1600),
("Uther Lightbringer", 4, 3, 1800), ("Guruprosad Rao", 7, 2, 2000), ("William Blastard", 8, 2, 2500),
("Crunchy Jones", 9, 3, 4000), ("Kelly Kapur", 10, 3, 1000), ("Andrew Smith", 11, 1, 800),
("Freddie Starboy", 12, 4, 1200), ("Maya Putintceva", 13, 3, 1500), ("Ariel McMaster", 14, 4, 500),
("Zull Djin", 1, 1, 500), ("Jane Doe", 8, 2, 600), ("Margareth Stranger", 9, 3, 600),
("John Robins", 10, 3, 1200), ("Mike Sigur", 11, 1, 900), ("Silvester Medius", 12, 4, 1000),
("Death Grips", 13, 3, 1800), ("Alice of Ozz", 14, 4, 800);

-- assigning skills to new developers

INSERT INTO developers_skills
VALUES (8, 7), (9, 4), (10, 9),	(11, 8), (12, 7), (13, 2), (13, 4),
(14, 5), (15, 6), (16, 1), (16, 7), (17, 2), (18, 2), (19, 3),
(20, 4), (21, 5), (22, 9), (23, 1), (24, 2), (25, 6), (26, 6),
(27, 6), (28, 2), (29, 3), (30, 1), (31, 4), (32, 4), (33, 1),
(30, 7), (27, 1), (25, 2), (23, 4), (21, 2), (19, 1), (26, 3);

-- create developers_projects join view

CREATE VIEW developers_projects AS
	SELECT developers.id AS dev_id, developers.salary AS dev_salary, 
	developers.company_id AS company_id, projects.id AS project_id,
	projects.cost as project_cost
	FROM developers LEFT JOIN projects ON developers.project_id = projects.id;

-- creating view with average salary

CREATE VIEW avg_salary AS
SELECT AVG(dev_salary) AS avg_salary, project_id, project_cost, company_id
FROM developers_projects
GROUP BY project_id;

-- selecting average salary on each project

SELECT a1.company_id, a1.project_id, a1.project_cost, a1.avg_salary  
FROM avg_salary a1
JOIN (
	SELECT company_id, MIN(project_cost) as project_cost
	FROM avg_salary
	GROUP BY company_id) AS a2
	ON a1.company_id = a2.company_id AND a1.project_cost = a2.project_cost;

-- dropping temporary views

DROP VIEW developers_projects;
DROP VIEW avg_salary;