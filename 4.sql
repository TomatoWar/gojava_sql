-- adding cost field to projects table

ALTER TABLE projects
ADD cost INT NOT NULL;

UPDATE projects
SET cost = 1000000;

UPDATE projects
SET cost = 12000000
WHERE id = 1;

UPDATE projects
SET cost = 2000000
WHERE id = 2;

UPDATE projects
SET cost = 3500000
WHERE id = 3;

UPDATE projects
SET cost = 800000000
WHERE id = 4;

-- tracking the result
SELECT id, name, cost FROM projects;